import prisma from './prisma';

export async function getPostsApiData() {
	const news = await prisma.post.findMany({});
	return news;
}

export async function getPostApiData(id) {
	const news = await prisma.post.findUnique({ where: { id: id } });
	return news;
}

export async function getPostsIdsApiData() {
	const news = await prisma.post.findMany({ select: { id: true } });
	return news.map(({ id }) => {
	  	return {
			params: {
			  	id: id,
			},
	  	};
	});
}