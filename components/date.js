import { parseISO, format } from 'date-fns';

export default function Date({ dateString, dateFormat }) {
  dateFormat = dateFormat ?? 'LLLL d, yyyy';
  // dateString = parseISO(dateString);
  const date = format(dateString, dateFormat); 
  return <time dateTime={dateString}>{date}</time>;
}