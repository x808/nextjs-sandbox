import Head from 'next/head'
import Layout from '../../components/layout'
import { getPostsIdsApiData, getPostApiData } from '../../lib/posts';
import Date from '../../components/date';


export async function getStaticPaths() {
    const paths = await getPostsIdsApiData();
    return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
    let news = await getPostApiData(params.id);
    return { props: { news } };
};

export default function FirstPost({ news }) {
    return (
        <Layout>
            <Head>
                <title>{ news.title } by { news.authorName }</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main>
                <h1 className="title">
                    { news.title }   
                </h1>
                <h2 dangerouslySetInnerHTML={{ __html: news.text }}></h2>
                <h3 className="title"><Date dateString={ news.createdAt } dateFormat="d/LL/yyyy" /> { news.authorName }</h3>  
            </main>
        </Layout>
    ); 
}