import Head from 'next/head';
import Link from 'next/link';
import Layout, { siteTitle } from '../components/layout';
import Date from '../components/date';
import utilStyles from '../styles/utils.module.css';
import { getPostsApiData } from '../lib/posts';

export async function getStaticProps() {
  const posts = await getPostsApiData();
  return { props: { posts } };
};

export default function Home({ posts }) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={`${ utilStyles.headingMd } ${ utilStyles.padding1px }`}>
        <h2 className={ utilStyles.headingLg }>News</h2>
        <ul className={ utilStyles.list }>
          {posts.map(({id, title, text, authorName, createdAt}) => (
            <li className={ utilStyles.listItem } key={ id }>
                <strong>
                  <Link href={`/posts/${id}`}>{ title }</Link>
                </strong>  
                <br />
                <small className={ utilStyles.lightText }>
                  <Date dateString={ createdAt } /> { authorName }
                </small>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  );
};