import Layout, { siteTitle } from '../components/layout';
import Head from 'next/head';
import utilStyles from '../styles/utils.module.css';

export async function getServerSideProps() {
    let data = await fetch('http://localhost:3000/api/hello');
    data = await data.json();
    return { props: { data } };
}

export default function Hello({ data }) {
    return(
        <Layout>
            <Head>
                <title>{ siteTitle }</title>
            </Head>
            <section className={`${ utilStyles.headingMd } ${ utilStyles.padding1px }`}>
                <h1 className="title">Massage: `{ data.message }`</h1>  
            </section>
        </Layout>
    );
};